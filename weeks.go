package main

import (
	"time"
)

func startOfWeek() time.Time {
	now := time.Now()
	startOfDay := time.Date(now.Year(), now.Month(), now.Day(), 0, 0, 0, 0, time.UTC)
	weekday := startOfDay.Weekday()

	switch weekday {
	case time.Sunday:
		return startOfDay.AddDate(0, 0, -6)
	case time.Monday:
		return startOfDay
	default:
		return startOfDay.AddDate(0, 0, -(int(weekday) - 1))
	}
}
