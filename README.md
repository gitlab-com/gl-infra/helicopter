# 🚁 Helicopter: a Workflow for AlertManager Silences and GitLab Issues

Helicopter is a small integration between Prometheus' AlertManager tool and GitLab issues.

## How does Helicopter work?

At present, Helicopter will generate a weekly reminder note on each GitLab issue referenced in the
comment field on a silence.

When executed more than once a week, the existing note will be updated. A new note is created each
week as a constant reminder to the issue subscribers that the technical debt exists (aka, the alert is
being silenced).

Future improvements to Helicopter may include some sort of validation of alerts, and weekly reporting
on alerts.

## Configuration

```shell
$ export GITLAB_TOKEN=s3cr3t
$ ./helicopter --help
usage: helicopter [<flags>]

Flags:
  --help  Show context-sensitive help (also try --help-long and --help-man).
  --alertmanager.url=http://alertmanager-internal.ops.gke.gitlab.net:9093
          Alert Manager URL
  --alertmanager.public.url=https://alerts.gitlab.net
          Alert Manager URL
$ ./helicopter --alertmanager.url=http://localhost:9093
Added note to https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11053
Added note to https://gitlab.com/gitlab-com/gl-infra/production/-/issues/2480
Added note to https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10928
Added note to https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/9627
Added note to https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/10657
Added note to https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8293
Added note to https://gitlab.com/gitlab-com/gl-infra/infrastructure/issues/8683
```

## Why the name "Helicopter"?

Helicopter is about dealing with silent alarms. "Silent Alarm" is the debut studio album by English rock band Bloc Party. Helicopter is the second track on that album. Obvious, isn't it?

## Hacking on Helicopter

1. Follow the instructions at <https://gitlab.com/gitlab-com/gl-infra/common-ci-tasks/-/blob/main/docs/developer-setup.md>
2. Run `scripts/prepare-dev-env.sh` to setup your development environment
3. Run `go build .` to build Helicopter
