package main

import (
	"context"
	"fmt"
	"net/url"
	"path"
	"time"

	clientruntime "github.com/go-openapi/runtime/client"
	"github.com/go-openapi/strfmt"
	"github.com/prometheus/alertmanager/api/v2/client"
	"github.com/prometheus/alertmanager/api/v2/client/silence"
	"github.com/prometheus/alertmanager/api/v2/models"
)

const (
	defaultAmHost      = "localhost"
	defaultAmPort      = "9093"
	defaultAmApiv2path = "/api/v2"
)

// NewAlertmanagerClient initializes an alertmanager client with the given URL.
func newAlertmanagerClient(amURL *url.URL) *client.Alertmanager {
	address := defaultAmHost + ":" + defaultAmPort
	schemes := []string{"http"}

	if amURL.Host != "" {
		address = amURL.Host // URL documents host as host or host:port
	}

	if amURL.Scheme != "" {
		schemes = []string{amURL.Scheme}
	}

	cr := clientruntime.New(address, path.Join(amURL.Path, defaultAmApiv2path), schemes)

	if amURL.User != nil {
		password, _ := amURL.User.Password()
		cr.DefaultAuthentication = clientruntime.BasicAuth(amURL.User.Username(), password)
	}

	return client.New(cr, strfmt.Default)
}

func listSilences(ctx context.Context, url *url.URL) (models.GettableSilences, error) {
	params := silence.NewGetSilencesParamsWithContext(ctx)
	params.WithTimeout(30 * time.Second)

	client := newAlertmanagerClient(url)

	silences, err := client.Silence.GetSilences(params)
	if err != nil {
		return nil, fmt.Errorf("failed to retrieve silences: %w", err)
	}

	return silences.GetPayload(), nil
}
