package main

import (
	"bytes"
	"fmt"
	"html/template"
	"time"

	"github.com/go-openapi/strfmt"
	"github.com/prometheus/alertmanager/api/v2/models"
)

type templateItem struct {
	Date       time.Time
	Silences   models.GettableSilences
	Conf       alertManagerConfig
	Backtick   string
	NoteMarker template.HTML
}

// Define a template.
const note = `
{{ .NoteMarker }}
## Active Alert Silences Related to this Issue, Week of {{ .Date | fdate -}}
{{"\n"}}
{{- range .Silences }}
* [
		{{- range $index, $matcher := .Silence.Matchers -}}
			{{- if $index}} {{ end -}}
			{{- $.Backtick }}{{ $matcher.Name -}}{{ if $matcher.IsRegex | deref_bool }}=~{{ else }}={{end}}{{- $matcher.Value -}}{{ $.Backtick -}}
		{{- end -}}
	]({{$.Conf.AlertManagerPublicURL}}/#/silences/{{.ID}}) since {{ .Silence.StartsAt | fdatetime -}}
{{- end}}

<small>Reported by [🚁 Helicopter](https://gitlab.com/gitlab-com/gl-infra/helicopter)</small>
`

var funcs = template.FuncMap{
	"fdatetime": func(datetime strfmt.DateTime) string {
		return time.Time(datetime).Format("2006-01-02 15:04Z")
	},
	"fdate": func(t time.Time) string {
		return t.Format("2006-01-02")
	},
	"deref_bool": func(v *bool) bool {
		return *v
	},
}

func noteForSilences(silences models.GettableSilences, sow time.Time, noteKey string, conf alertManagerConfig) (string, error) {
	// Create a new template and parse the note into it.
	t, err := template.New("note").Funcs(funcs).Parse(note)
	if err != nil {
		return "", fmt.Errorf("failed to parse template: %w", err)
	}

	buf := &bytes.Buffer{}

	err = t.Execute(buf, templateItem{
		Date:       sow,
		Silences:   silences,
		Conf:       conf,
		Backtick:   "`",
		NoteMarker: template.HTML(noteKey), //nolint:gosec
	})
	if err != nil {
		return "", fmt.Errorf("failed to parse template: %w", err)
	}

	return buf.String(), nil
}
