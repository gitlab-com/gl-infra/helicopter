package main

import (
	"errors"
	"fmt"
	"os"
	"regexp"
	"strconv"
	"strings"

	"github.com/xanzy/go-gitlab"
)

var errNote = errors.New("note error")

func findExistingNote(client *gitlab.Client, project string, issueIID int, noteKey string) (*gitlab.Note, error) {
	page := 1

	for {
		opt := &gitlab.ListIssueNotesOptions{
			ListOptions: gitlab.ListOptions{
				Page:    page,
				PerPage: 100,
			},
		}

		notes, response, err := client.Notes.ListIssueNotes(project, issueIID, opt)
		if err != nil {
			return nil, fmt.Errorf("unable to list notes: %w", errNote)
		}

		for _, note := range notes {
			if strings.Contains(note.Body, noteKey) {
				return note, nil
			}
		}

		if response.TotalPages > page {
			page = page + 1
			continue
		}

		return nil, nil
	}
}

func extractProjectAndIssueIDFromURL(issueURL string) (project string, issueIID int, err error) {
	re := regexp.MustCompile(`https://gitlab.com/((?:[\w][\w-]*)(?:/[\w][\w-]*)+)(?:/\-)?/issues/(\d+)(?:$|\?|#)`)
	matches := re.FindStringSubmatch(issueURL)

	if len(matches) < 3 {
		return "", 0, fmt.Errorf("failed to grok issue URL: %s (%+v): %w", issueURL, matches, errNote)
	}

	project = matches[1]

	issueIID, err = strconv.Atoi(matches[2])
	if err != nil {
		return "", 0, fmt.Errorf("issue URL issue_iid is not an integer: %w", err)
	}

	return project, issueIID, nil
}

func noteOnIssue(project string, issueIID int, noteKey string, noteText string) error {
	client, err := gitlab.NewClient(os.Getenv("GITLAB_TOKEN"))
	if err != nil {
		return fmt.Errorf("unable to create a GitLab client: %w", err)
	}

	existingNote, err := findExistingNote(client, project, issueIID, noteKey)
	if err != nil {
		return fmt.Errorf("unable to find note: %w", err)
	}

	if existingNote != nil {
		_, _, err = client.Notes.UpdateIssueNote(project, issueIID, existingNote.ID, &gitlab.UpdateIssueNoteOptions{Body: &noteText})
		if err != nil {
			return fmt.Errorf("unable to update note: %w", err)
		}

		return addLabel(client, project, issueIID, "alertmanager-silence")
	}

	_, _, err = client.Notes.CreateIssueNote(project, issueIID, &gitlab.CreateIssueNoteOptions{Body: &noteText})
	if err != nil {
		return fmt.Errorf("unable to create note: %w", err)
	}

	return addLabel(client, project, issueIID, "alertmanager-silence")
}

func addLabel(gitlabClient *gitlab.Client, projectPath string, issueIID int, label string) error {
	issue, _, err := gitlabClient.Issues.GetIssue(projectPath, issueIID)
	if err != nil {
		return fmt.Errorf("failed to retrieve issues: %w", err)
	}

	for _, existingLabel := range issue.Labels {
		if existingLabel == label {
			return nil
		}
	}

	newLabels := append(issue.Labels, label)
	issuePatch := &gitlab.UpdateIssueOptions{Labels: &newLabels}

	_, _, err = gitlabClient.Issues.UpdateIssue(projectPath, issueIID, issuePatch)
	if err != nil {
		return fmt.Errorf("unable to update issue: %w", err)
	}

	return nil
}
