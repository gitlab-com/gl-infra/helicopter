package main

import "testing"

var extractTestFixtures = []struct {
	name         string
	issueURL     string
	wantProject  string
	wantIssueIID int
	wantErr      bool
}{
	{
		name:         "Two tier",
		issueURL:     "https://gitlab.com/gitlab-com/scalability/-/issues/145",
		wantProject:  "gitlab-com/scalability",
		wantIssueIID: 145,
		wantErr:      false,
	},
	{
		name:         "Three tier",
		issueURL:     "https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/145",
		wantProject:  "gitlab-com/gl-infra/scalability",
		wantIssueIID: 145,
		wantErr:      false,
	},
	{
		name:         "Two tier, old style",
		issueURL:     "https://gitlab.com/gitlab-com/scalability/issues/145",
		wantProject:  "gitlab-com/scalability",
		wantIssueIID: 145,
		wantErr:      false,
	},
	{
		name:         "Three tier, old style",
		issueURL:     "https://gitlab.com/gitlab-com/gl-infra/scalability/issues/145",
		wantProject:  "gitlab-com/gl-infra/scalability",
		wantIssueIID: 145,
		wantErr:      false,
	},
	{
		name:         "Single tier invalid",
		issueURL:     "https://gitlab.com/scalability/issues/145",
		wantProject:  "",
		wantIssueIID: 0,
		wantErr:      true,
	},
	{
		name:         "Issues",
		issueURL:     "https://gitlab.com/gitlab-com/gl-infra/scalability/issues",
		wantProject:  "",
		wantIssueIID: 0,
		wantErr:      true,
	},
}

func Test_extractProjectAndIssueIDFromURL(t *testing.T) {
	t.Parallel()

	for _, tt := range extractTestFixtures {
		t.Run(tt.name, func(t *testing.T) {
			t.Parallel()

			gotProject, gotIssueIID, err := extractProjectAndIssueIDFromURL(tt.issueURL)
			if (err != nil) != tt.wantErr {
				t.Errorf("extractProjectAndIssueIDFromURL() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if gotProject != tt.wantProject {
				t.Errorf("extractProjectAndIssueIDFromURL() gotProject = %v, want %v", gotProject, tt.wantProject)
			}

			if gotIssueIID != tt.wantIssueIID {
				t.Errorf("extractProjectAndIssueIDFromURL() gotIssueIID = %v, want %v", gotIssueIID, tt.wantIssueIID)
			}
		})
	}
}
