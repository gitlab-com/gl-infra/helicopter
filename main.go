package main

import (
	"context"
	"fmt"
	"log"
	"os"
)

func main() {
	config, ok := getConfig()
	if !ok {
		os.Exit(1)
		return
	}

	silences, err := listSilences(context.Background(), config.AlertManagerURL)
	if err != nil {
		panic(err)
	}

	sow := startOfWeek()
	noteKey := fmt.Sprintf("<!-- Silenced alarms: %s -->", sow.Format("2006-01-02"))
	errored := false

	for issueURL, silences := range groupSilencesByIssues(silences) {
		noteText, err := noteForSilences(silences, sow, noteKey, config)
		if err != nil {
			log.Printf("unable to generate a note for silence: %v", err)

			continue
		}

		project, issueIID, err := extractProjectAndIssueIDFromURL(issueURL)
		if err != nil {
			errored = true

			logError(err)

			continue
		}

		err = noteOnIssue(project, issueIID, noteKey, noteText)
		if err != nil {
			errored = true

			logError(err)

			continue
		}

		fmt.Printf("Added note to %s\n", issueURL)
	}

	if errored {
		fmt.Println("Errors were encountered during execution")
		os.Exit(1)
	}
}

func logError(err error) {
	log.Printf("unable to generate a note for silence: %v", err)
}
