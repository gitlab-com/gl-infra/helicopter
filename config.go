package main

import (
	"net/url"

	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	alertManagerURL       = kingpin.Flag("alertmanager.url", "Alert Manager URL").Default("http://alertmanager-internal.ops.gke.gitlab.net:9093").URL()
	alertManagerPublicURL = kingpin.Flag("alertmanager.public.url", "Alert Manager URL").Default("https://alerts.gitlab.net").URL()
)

type alertManagerConfig struct {
	AlertManagerURL       *url.URL
	AlertManagerPublicURL *url.URL
}

func getConfig() (alertManagerConfig, bool) {
	kingpin.Parse()

	conf := alertManagerConfig{
		AlertManagerURL:       *alertManagerURL,
		AlertManagerPublicURL: *alertManagerPublicURL,
	}

	return conf, true
}
