package main

import (
	"regexp"
	"sort"
	"time"

	"github.com/prometheus/alertmanager/api/v2/models"
	"mvdan.cc/xurls/v2"
)

var gitlabIssueURLMatcher = regexp.MustCompile(`^https://gitlab.com/.*/issues/\d+`)

func findIssuesURLsForSilence(silence *models.GettableSilence) []string {
	rxStrict := xurls.Strict()
	allURLs := rxStrict.FindAllString(*silence.Silence.Comment, -1)
	results := []string{}

	for _, u := range allURLs {
		if gitlabIssueURLMatcher.MatchString(u) {
			results = append(results, u)
		}
	}

	return results
}

type silenceSorter struct {
	silences models.GettableSilences
}

func (s *silenceSorter) Len() int {
	return len(s.silences)
}

func (s *silenceSorter) Swap(i, j int) {
	s.silences[i], s.silences[j] = s.silences[j], s.silences[i]
}

func (s *silenceSorter) Less(i, j int) bool {
	si := s.silences[i]
	sj := s.silences[j]

	return time.Time(*si.StartsAt).Before(time.Time(*sj.StartsAt))
}

// Group silences by references issues.
func groupSilencesByIssues(silences models.GettableSilences) map[string]models.GettableSilences {
	mappedSilences := map[string]models.GettableSilences{}

	for _, silence := range silences {
		if *silence.Status.State != "active" {
			continue
		}

		issueURLs := findIssuesURLsForSilence(silence)

		for _, issueURL := range issueURLs {
			mappedSilences[issueURL] = append(mappedSilences[issueURL], silence)
		}
	}

	// Sort by StartsAt
	for _, v := range mappedSilences {
		sort.Sort(&silenceSorter{v})
	}

	return mappedSilences
}
